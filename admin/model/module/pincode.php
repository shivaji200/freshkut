<?php
class ModelModulePincode extends Model {
	public function addPincode($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "pincode SET freshkut_available = '" . $this->db->escape($data['status']) . "', office_name = '" . $this->db->escape($data['name']) . "', pincode = '" . $this->db->escape($data['pincode']) . "', taluka = '" . $this->db->escape($data['taluka']) . "', district = '" . $this->db->escape($data['district']) . "', postal_division = '" . $this->db->escape($data['postal_division']) . "'");

		$this->cache->delete('pincode');
	}

	public function editPincode($Pincode_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "pincode SET freshkut_available = '" . $this->db->escape($data['status']) . "', office_name = '" . $this->db->escape($data['name']) . "', pincode = '" . $this->db->escape($data['pincode']) . "', taluka = '" . $this->db->escape($data['taluka']) . "', district = '" . $this->db->escape($data['district']) . "', postal_division = '" . $this->db->escape($data['postal_division']) . "' WHERE id = '" . (int)$Pincode_id . "'");

		$this->cache->delete('pincode');
	}

	public function deletePincode($Pincode_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "pincode WHERE id = '" . (int)$Pincode_id . "'");

		$this->cache->delete('pincode');
	}

	public function getPincode($Pincode_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "pincode WHERE id = '" . (int)$Pincode_id . "'");

		return $query->row;
	}

	public function getPincodes($data = array()) {
		$sql = "SELECT p.id,p.office_name, p.pincode,p.freshkut_available  FROM " . DB_PREFIX . "pincode p ";

		$sort_data = array(
			'p.office_name',
			'p.pincode',
			'p.freshkut_available'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY p.office_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getPincodesByCountryId($country_id) {
		$Pincode_data = $this->cache->get('pincode.' . (int)$country_id);

		if (!$Pincode_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pincode WHERE country_id = '" . (int)$country_id . "' AND status = '1' ORDER BY name");

			$Pincode_data = $query->rows;

			$this->cache->set('pincode.' . (int)$country_id, $Pincode_data);
		}

		return $Pincode_data;
	}

	public function getTotalPincodes() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "pincode");

		return $query->row['total'];
	}

	public function getTotalPincodesByCountryId($country_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "pincode WHERE country_id = '" . (int)$country_id . "'");

		return $query->row['total'];
	}

	public function checkPincode($pincode) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "pincode WHERE pincode = '" . (int)$pincode . "' AND freshkut_available = 'Available'");

		return $query->row['total'];
	}
}