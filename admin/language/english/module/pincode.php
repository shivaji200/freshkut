<?php

//Heading
$_['heading_title'] 	= 'Pincode';

//Text
$_['text_module']		= 'Module';
$_['text_list']			= 'Pincode List';
$_['text_cepInicial'] 	= 'ZIP Code Start';
$_['text_cepFinal'] 	= 'ZIP Code End';
$_['text_acao'] 		= 'Action';
$_['text_salvar'] 		= 'Save';
$_['text_cancelar'] 	= 'Cancel';
$_['text_remover'] 		= 'Remove';
$_['text_adicionar'] 	= 'Insert';
$_['text_situacao']		= 'Status:';
$_['text_success']		= 'Success';
$_['text_office'] 		= 'Office';
$_['text_pincode'] 		= 'Pincode';
$_['text_status']		= 'Status';
$_['text_action']		= 'Action';
$_['button_add'] 		= 'Add';
$_['button_edit']		= 'Edit';
$_['button_delete']		= 'Delete';
$_['text_enabled']		= 'Available';
$_['text_disabled']		= 'Not Available';
$_['error_name']		= 'Office name is required';
$_['error_pincode']		= 'Pincode is required';
$_['text_confirm']		= 'Are You Sure?';


//Entry
$_['entry_habilitado']    = 'Enabled';
$_['entry_desabilitado']  = 'Disabled';

//Add Pincode
$_['text_add']			= 'Add Pincode';
$_['text_office']		= 'Office Name';
$_['text_pincode'] 		= 'Pincode';
$_['text_taluka'] 		= 'Taluka';
$_['text_district'] 	= 'District';
$_['text_division'] 	= 'Postal Division';
$_['text_available'] 	= 'Freshkut Available ?';

//Edit Pincode
$_['text_edit']			= 'Edit Pincode';
$_['text_list']			= 'Pincode List';
$_['text_cepInicial'] 	= 'ZIP Code Start';
$_['text_cepFinal'] 	= 'ZIP Code End';