<?php
// Text
$_['text_information']  		= 'Information';
$_['text_service_guarantee']    = '<span>Service & Quality</span> GUARANTEED';
$_['text_ontime']        		= '<span>On Time </span><br>Delivery PROMISE';
$_['text_prefferd_delivery']    = '<span>Preferred Delivery </span> <br>Time AVAILABLE';
$_['text_deliverymin']       	= '<span>Delivered In 90 </span><br>Min GUARANTEED';
$_['text_our_mission']   		= 'Our Mission';
$_['text_customer_service']   	= 'Customer Service';
$_['text_policy']   			= 'Policy';
$_['text_gofresho']   			= 'Freshkut';
$_['text_powered']      		= 'Copyright © 2016, Freshkut.com, All Rights Reserved.';
$_['text_sitemap']      		= 'Site Map';
$_['text_about_us'] 			= 'About Us';
$_['text_delivery_info']      	= 'Delivery Information';
$_['text_faq']    				= 'FAQ';
$_['text_howworks']      		= 'How it Works';
$_['text_cancel']      			= 'Cancel & Return Policy';
$_['text_terms']        		= 'Terms & Conditions';
$_['text_privacy']     			= 'Privacy Policy';
$_['text_social']     			= 'Social Media Disclaimer';
$_['text_contactus']     		= 'Contact Us';
$_['text_careers']     			= 'Careers';
$_['text_bulkorder']     		= 'Bulk Orders';
$_['text_partner']     			= 'Partner with Us';
$_['text_accont']     			= 'My Account';