<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/custom.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/freshcut/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<?php if(!empty($IsHome)){ ?>
<div class="container-fluidc header-bg">
<nav id="top">
    <div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="top_contact"><strong>Call Us : <i class="fa fa-phone"></i>+91 9310-123-123</strong></div>
            <?php if($location) { ?>
            <div class="location"><?php echo $location; ?> <a href="<?php echo $location_link;?>" class="popup_loc"> Change location</a></div>
            <?php } ?>
        </div>
        <div class="col-sm-6">
            <div id="top-links" class="nav">
            <ul class="list-inline pull-right">
                <li class="dropdown"><a href="javascript:(void);" title="My Account" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md">My Account</span> <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <?php if ($logged) { ?>
                    <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                    <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                    <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                    <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                    <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                    <?php } else { ?>
                    <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                    <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                    <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
                <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
                <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>
            </ul>
        </div>
        <div class="social_icon pull-right">
            <ul class="nav navbar-nav navbar-right social-top">
                <li class="google"><a href="https://plus.google.com/105931911408580977766"><i class="fa fa-google-plus"></i></a></li>
                <li class="twitter1"><a href="https://twitter.com/Fresh4Kut"><i class="fa fa-twitter"></i></a></li>
                <li class="facebook1"><a href="https://www.facebook.com/FreshKut"><i class="fa fa-facebook"></i></a></li>
                <li class="youtube1"><a href="https://www.youtube.com/channel/UCg0MtRo7m5JtSlY35EJUR7A"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </div>
        <div class="cart_displaytop pull-right"><?php echo $cart; ?></div>
        </div>
    </div>

    </div>
</nav>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <?php if ($logo) { ?>
                    <a class="navbar-brand" href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
                    <?php } else { ?>
                    <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                    <?php } ?>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo $vegetables ?>">Vegetables</a></li>
                        <li><a href="<?php echo $fruits ?>">Fruits </a></li>
                    </ul>
                    <div class="pull-right search_input">
                    <?php echo $search; ?>
                    </div>
                </div>
                <!--/.nav-collapse -->
            </div>
            <!--/.container-fluid -->
        </nav>
    </div>
    <div class="container maxwidth">
        <div class="freshcut-head">
            <h1 class="text-uppercase">FreshKut</h1>
           <h3 class="text-uppercase">Cut today , delivered today</h3>
            <a href="<?php echo $home; ?>products">Order Now</a>
        </div>
    </div>
</div>
<?php }else{ ?>
<nav id="top">
    <div class="container">
    <div class="row">
        <div class="col-sm-6">
            
            <div class="top_contact"><strong>Call Us : <i class="fa fa-phone"></i>+91 9310-123-123</strong></div>
            <?php if($location) { ?>
            <div class="location"><?php echo $location; ?> <a href="<?php echo $location_link;?>" class="popup_loc"> Change location</a></div>
            <?php } ?>
        </div>
        <div class="col-sm-6">
            <div id="top-links" class="nav">
            <ul class="list-inline pull-right">
                <li class="dropdown"><a href="javascript:(void);" title="My Account" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md">My Account</span> <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <?php if ($logged) { ?>
                    <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                    <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                    <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                    <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                    <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                    <?php } else { ?>
                    <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                    <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                    <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
                <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
                <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>
            </ul>
        </div>
        <div class="social_icon pull-right">
            <ul class="nav navbar-nav navbar-right social-top">
                <li><a href="https://plus.google.com/105931911408580977766"><img src="<?=DIR_IMAGE_PATH?>facebook-icon.png" alt="Facebook" /></a></li>
                <li><a href="https://twitter.com/Fresh4Kut"><img src="<?=DIR_IMAGE_PATH?>twitter-icon.png" alt="Twitter" /></a></li>
                <li><a href="https://www.facebook.com/FreshKut"><img src="<?=DIR_IMAGE_PATH?>google-icon.png" alt="Google Plus" /></a></li>
                <li><a href="https://www.youtube.com/channel/UCg0MtRo7m5JtSlY35EJUR7A"><img src="<?=DIR_IMAGE_PATH?>youtube-icon.png" alt="Youtube" /></a></li>
            </ul>
        </div>
        <div class="cart_displaytop pull-right"><?php echo $cart; ?></div>
        </div>
    </div>

    </div>
    </nav>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <?php if ($logo) { ?>
                    <a class="navbar-brand" href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
                    <?php } else { ?>
                    <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                    <?php } ?>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo $vegetables ?>">Vegetables</a></li>
                        <li><a href="<?php echo $fruits ?>">Fruits </a></li>
                    </ul>
                    <div class="pull-right search_input">
                    <?php echo $search; ?>
                    </div>
                </div>
                <!--/.nav-collapse -->
            </div>
            <!--/.container-fluid -->
        </nav>
    </div>
<?php } ?>
