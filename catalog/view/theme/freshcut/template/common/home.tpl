<?php echo $header; ?>
<div class="container section-1">
    <h1 class="text-center text-uppercase">"WHAT'S New On Fresh Kut"</h1>
    <h2 class="text-center text-uppercase">FINALLY HAS AN EASY ANSWER FOR DAILY FOODS</h2>
    <div class="row">
        <div class="col-md-4" align="center"> <img src="<?=DIR_IMAGE_PATH?>/one-img.png" class="img-responsive" alt="" />
        </div>
        <div class="col-md-4" align="center"> <img src="<?=DIR_IMAGE_PATH?>/two-img.png" class="img-responsive" alt="" />
        </div>
        <div class="col-md-4" align="center"> <img src="<?=DIR_IMAGE_PATH?>/three-img.png" class="img-responsive" alt="" />
        </div>
    </div>
    <div class="row redborder">
        <div class="col-md-3" align="center"><a href="#"><img src="<?=DIR_IMAGE_PATH?>/g1.png" class="img-responsive" alt="" /></a></div>
        <div class="col-md-3" align="center"><a href="#"><img src="<?=DIR_IMAGE_PATH?>/g2.png" class="img-responsive" alt="" /></a></div>
        <div class="col-md-3" align="center"><a href="#"><img src="<?=DIR_IMAGE_PATH?>/g3.png" class="img-responsive" alt="" /></a></div>
        <div class="col-md-3" align="center"><a href="#"><img src="<?=DIR_IMAGE_PATH?>/home_page_van_new.png" class="img-responsive" alt="" /></a></div>

    </div>
</div>

<!--<?php echo $content_top; ?> -->
<div class="container-fluid spacing-top-bot7">
    <div class="container search-section">
        <div class="row">
            <div class="col-md-8">
                <div class="close-toyou">
                    <h3>Close to you</h3>
					<div id="error-msg"></div>
                    <input type="text" name="" id="pincode" value="" placeholder="Enter Postal Code or City" class="search-input"/>
                    <input type="button" name="" value="GO" id="checkPinCode" class="search-btn"/>
                </div>
            </div>
            <div class="col-md-4" style="position:relative;">
                <div class="whenyou">
                    <h3>READY WHEN YOU ARE</h3>
                    <p>Buy Fresh Fruits & Vegetables at Best Price in Delhi-NCR India.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
                    $(document).ready(function(){
                        $('#checkPinCode').click(function(){
                            var pincode = $('#pincode').val();

                            if(pincode == ''){
                                var error = 'Please enter Pincode!';
                            }

                            if(error != null){
                                $('#error-msg').html('');
                                $('#error-msg').html('<b style=\"color:red\">' + error + '</b>');
                            } else {

                                var dataString = 'pincode='+ pincode;
                                $.ajax({
                                    url: 'index.php?route=common/home/checkPincode',
                                    type: 'post',
                                    data: dataString,
                                    success: function(html) {
                                       $('#error-msg').html(html);
                                    }

                                });
                            }

                        })
                    });
                </script>

<?php if($location == '') { ?>
<div id="popup_iiner">
<div id="popup">
<!-- <a class="close_1">X</a> -->
    <div class="popup_under">
    <div class="remove_icon"><i class="fa fa-remove"></i></div>
        <div class="popup_logo">
            <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
        </div>
        <div class="row spacing12">
            <div class="col-sm-4 text-center divider1">
                <div class="text01"></div>
                <p>Same-Day Delivery</p>
                <p>Quality You'll Love</p>
                <p>On Time Every Time</p>
            </div>
            <div class="col-sm-8">
                <div class="row right_searcharea">
                    <div class="col-sm-12 form-group">
                        <label>Your City</label>
                        <select name="" class="inputstyle1">
                            <option value="">Delhi NCR</option>
                        </select>
                    </div>
                    <div class="col-sm-12 form-group" id="searchpin">
                        <label>Where do you want us to deliver your basket?</label>
                        <input type="text" value="" name="search" class="inputstyle2 search-autocomplete" placeholder="Search by Area or Apartment or Pincode *">
                        <div id="result-search-autocomplete" class="result-search-autocomplete">
                            <ul class="show-result"></ul>
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                    <a class="btn start_shpippingbtn">Start Shopping</a>
                    <a class="btn skip_btn">Skip & Explore</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script  type="text/javascript" >
    var width_search = document.getElementById("searchpin").offsetWidth;
    //$('.result-search-autocomplete').css({"width":width_search});
    $('.search-autocomplete').keyup(function(event) {
        /* Act on the event */
        $('.result-search-autocomplete  ul').css({"overflow" : "hidden"});
        var search = $('#searchpin input[name=search]').val();
        $.ajax({
          method: "GET",
          url: "<?php echo $search_action; ?>",
          data: { search : search }
        }).done(function( result ) {
            var html = '';
            if(result && search != '')
            {
                var count = 0
                $.each(JSON.parse(result), function( index, value ) {
                    html += '<li>';
                    html += '<a href="'+value.href.replace('amp;', '')+'">';
                    html += '<div class="row">';
                    //html += '<div class="col-md-3 row-result-search-autocomplete-image">';
                    //html += '<img class="result-search-autocomplete-image" src="'+value.thumb+'">';
                    //html += '</div>';
                    html += '<div class="col-md-12 result-info">';
                    html += '<h4>'+value.location_name+'</h4>';
                    html += '</div>';
                    //html += '<div style="text-align:right" class="col-md-3 result-button">';
                    //html += '<button type="button" class="btn tagdattruoc"><?php echo $button_cart; ?></button>';
                    //html += '<h6>Xem them</h6>';
                    //html += '</div>';
                    html += '</div>';
                    html += '</a>';
                    html += '</li>';
                    count++;
                });
                    $('.result-search-autocomplete').css({"display":"block"});
                    if(count > 5)
                    {
                        $('.result-search-autocomplete  ul').css({"overflow" : "scroll"});
                    }else{
                        $('.result-search-autocomplete  ul').css({"overflow" : "hidden"});
                    }

            }else{
                html = '<li><div class="row"><div class="col-md-12 result-info"><h4>No Results</h4></div></div></li>';
                $('.result-search-autocomplete').css({"display":"block"});
                //$('.result-search-autocomplete').css({"display":"none"});
            }
                if(result.length == 2 && search != ''){
                    html = '<li><div class="row"><div class="col-md-12 result-info"><h4>No Results</h4></div></div></li>';
                    $('.result-search-autocomplete').css({"display":"block"});
                }

            $('.show-result').html(html);
        });
    });
</script>
<style type="text/css" media="screen">
.result-search-autocomplete
{
    display: none;
    position: absolute;
    z-index: 1000;
    background-color: #FFF;
    border: 1px solid #ddd;
    top:59px;
    max-height:468px;
}
.result-search-autocomplete h4
{
      display: block;
      width: 72%;
      line-height: 1.3em;
      color: #333;
      font-size: 14px;
      font-weight: 700;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
}
.result-search-autocomplete h5
{
    font-size: 14px;
    margin-top: 8px;
    color: red;
}
.result-search-autocomplete h5 i
{
    color: #999;
    font-style: normal;
    font-size: 11px;
    text-decoration: line-through;
}
.result-search-autocomplete h6
{
    text-transform: uppercase;
    font-size: 9px;
    font-weight: 700;
    color: #0876e6;
    display: block;
    margin-top: 8px;
    text-align: right;
}
.result-search-autocomplete ul, li
{
    list-style-type: none;
    margin: 0;
    padding: 0;
}
.result-search-autocomplete-image
{
    height: 50px;
    padding-left: 15px;
}
.result-search-autocomplete > ul
{
    max-height:468px;
    overflow: hidden;
    width: 302px;
    /*overflow: scroll;
    overflow-x:none;*/
}
.result-search-autocomplete > ul >li >a
{
    position: relative;
    display: block;
    overflow: hidden;
    padding: 6px;
    text-decoration: none;
}
.result-search-autocomplete > ul >li 
{
    display: block;
    background: #fff;
    overflow: hidden;
    list-style: none;
    border-bottom: 1px dotted #ccc;
    float: none;
}
.result-search-autocomplete > ul >li > a:hover button
{
    color: #FFF;
}
.tagdattruoc {
  background: #3498db;
  border: 1px solid #0679c6;
  font-size: 11px;
  color: #fff;
  border-radius: 0px;
  margin-top: 18px;
}
.tagdattruoc :hover
{
    color: #FFF;
}
@media (max-width: 767px) {
        .result-button {
            width: 30%;
            float: left;
        }
        .row-result-search-autocomplete-image
        {
            width: 30%;
            float: left;
        }
        .result-info
        {
            width: 40%;
            float: left;
        }
    }

</style>
<?php } ?>
 <script type="text/javascript">
     $(document).ready(function(){
        $(".remove_icon").click(function(){
            $("#popup_iiner").hide();
        });
     });
 </script>

<?php echo $footer; ?>