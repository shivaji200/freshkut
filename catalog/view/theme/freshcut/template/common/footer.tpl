<div class="container-fluid footer spacing-top-bot7">
    <div class="container">
        <div class="row">
            <div class="col-md-3 step1" align="center">
                <div class="col-md-4 padding-left5"><img src="<?=DIR_IMAGE_PATH?>step1.png" class="" alt="" /></div>
                <div class="col-md-8 padding-left5">
                    <h3><?php echo $text_service_guarantee; ?></h3>
                </div>
            </div>
            <div class="col-md-3 step1">
                <div class="col-md-4 padding-left5"><img src="<?=DIR_IMAGE_PATH?>step2.png" class="" alt="" /></div>
                <div class="col-md-8 padding-left5">
                    <h3><?php echo $text_ontime; ?></h3>
                </div>
            </div>
            <div class="col-md-3 step1">
                <div class="col-md-4 padding-left5"><img src="<?=DIR_IMAGE_PATH?>step3.png" class="" alt="" /></div>
                <div class="col-md-8 padding-left5">
                    <h3><?php echo $text_prefferd_delivery; ?></h3>
                </div>
            </div>
            <div class="col-md-3 step1">
                <div class="col-md-4 padding-left5"><img src="<?=DIR_IMAGE_PATH?>step4.png" class="" alt="" /></div>
                <div class="col-md-8 padding-left5">
                    <h3><?php echo $text_deliverymin; ?></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row footer-links">
            <div class="col-md-9">
                <div class="col-md-3">
                    <h3><?php echo $text_our_mission; ?></h3>
                    <p>Freshkut believes that fresh ingredients and quality food lead to nutritious, delicious meals. we travel near and far to bring our customers the very best right to their front door.</p>
                </div>
                <div class="col-md-3">
                    <h3><?php echo $text_customer_service; ?></h3>
                    <p>Phone : <?php echo $telephone; ?></p>
                    <p>Email : <?php echo $email; ?></p>
                    <p>Address : <?php echo $address; ?></p>
                </div>
                <div class="col-md-3">
                    <h3><?php echo $text_information; ?></h3>
                    <ul>
                        <li><a href="<?php echo $aboutus; ?>"><?php echo $text_about_us; ?></a></li>
                        <li><a href="<?php echo $delivery_info; ?>"><?php echo $text_delivery_info; ?></a></li>
                        <li><a href="<?php echo $faq; ?>"><?php echo $text_faq; ?></a></li>
                        <li><a href="<?php echo $how_works; ?>"><?php echo $text_howworks; ?></a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3><?php echo $text_policy; ?></h3>
                    <ul>
                        <li><a href="<?php echo $cancel; ?>"><?php echo $text_cancel; ?></a></li>
                        <li><a href="<?php echo $terms; ?>"><?php echo $text_terms; ?></a></li>
                        <li><a href="<?php echo $privacy; ?>"><?php echo $text_privacy; ?></a></li>
                        <li><a href="<?php echo $social; ?>"><?php echo $text_social; ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <h3><?php echo $text_gofresho; ?></h3>
                <ul>
                    <li><a href="<?php echo $contact; ?>"><?php echo $text_contactus; ?></a></li>
                    <li><a href="<?php echo $careers; ?>"><?php echo $text_careers; ?></a></li>
                    <li><a href="<?php echo $bulkorder; ?>"><?php echo $text_bulkorder; ?></a></li>
                    <li><a href="<?php echo $affiliate; ?>"><?php echo $text_partner; ?></a></li>
                </ul>
            </div>
        </div>
        <div class="row footer-social">
            <div class="col-md-4 col-md-offset-4">
            <div class="social">
              <ul>
                  <li><a href="https://www.facebook.com/FreshKut"><i class="fa fa-lg fa-facebook"></i></a></li>
                  <li><a href="https://twitter.com/Fresh4Kut"><i class="fa fa-lg fa-twitter"></i></a></li>
                  <li><a href="https://www.printrest.com"><i class="fa fa-lg fa-pinterest"></i></a></li>
<!--                   <li><a href="#"><i class="fa fa-lg fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-lg fa-google-plus"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-github"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-flickr"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-vimeo-square"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-stack-overflow"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-dropbox"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-tumblr"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-dribbble"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-skype"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-stack-exchange"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-youtube"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-xing"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-rss"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-foursquare"></i></a></li>
                  <li><a href="#"><i class="fa fa-lg fa-youtube-play"></i></a></li> -->
              </ul>
          </div>
            </div>
            <div class="col-md-4">
              <a href="#"><img src="<?=DIR_IMAGE_PATH?>google-play.jpg" alt="" class="img-responsive pull-left"/></a>
              <a href="#"><img src="<?=DIR_IMAGE_PATH?>appstore.jpg" alt="" class="img-responsive pull-left" /></a>
            </div>
        </div>
        <div class="row footerlogo">
            <div class="col-md-5">
                <p><?php echo $powered; ?></p>
            </div>
            <div class="col-md-7"><img src="<?=DIR_IMAGE_PATH?>footer-logo.png" class="img-responsive" alt="" /></div>
        </div>
    </div>
</div>





<?php /* ?>
<footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p><?php echo $powered; ?></p>
  </div>
</footer>
<?php */ ?>
</body></html>