<div class="container-fluid product-section-1">
    <div class="container spacing-top-bot7">
        <div class="row">
            <?php foreach ($products as $product) { ?>
            <div class="col-md-4 text-center">
                <div class="featured_box">
                    <h3><?php echo $product['name']; ?></h3>
                    <div align="center"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive"/></a></div>
                    <p><?php echo $product['description']; ?></p>
                    <a href="<?php echo $product['href']; ?>">view more</a> 
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>