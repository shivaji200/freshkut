<?php echo $header; ?>
<div class="container section-1">
    <h1 class="text-center text-uppercase">"WHAT'S New On Fresh Cut"</h1>
    <h2 class="text-center text-uppercase">FINALLY HAS AN EASY ANSWER FOR DAILY FOODS</h2>
    <div class="row">
        <div class="col-md-4" align="center"> <img src="<?=DIR_IMAGE_PATH?>/one-img.png" class="img-responsive" alt="" />
        </div>
        <div class="col-md-4" align="center"> <img src="<?=DIR_IMAGE_PATH?>/two-img.png" class="img-responsive" alt="" />
        </div>
        <div class="col-md-4" align="center"> <img src="<?=DIR_IMAGE_PATH?>/three-img.png" class="img-responsive" alt="" />
        </div>
    </div>
    <div class="row redborder">
        <div class="col-md-3" align="center"><a href="#"><img src="<?=DIR_IMAGE_PATH?>/g1.png" class="img-responsive" alt="" /></a></div>
        <div class="col-md-3" align="center"><a href="#"><img src="<?=DIR_IMAGE_PATH?>/g2.png" class="img-responsive" alt="" /></a></div>
        <div class="col-md-3" align="center"><a href="#"><img src="<?=DIR_IMAGE_PATH?>/g3.png" class="img-responsive" alt="" /></a></div>
        <div class="col-md-3" align="center"><a href="#"><img src="<?=DIR_IMAGE_PATH?>/home_page_van_new.png" class="img-responsive" alt="" /></a></div>

    </div>
</div>

<?php echo $content_top; ?>
<div class="container-fluid spacing-top-bot7">
    <div class="container search-section">
        <div class="row">
            <div class="col-md-8">
                <div class="close-toyou">
                    <h3>Close to you</h3>
					<div id="error-msg"></div>
                    <input type="text" name="" id="pincode" value="" placeholder="Enter Postal Code or City" class="search-input"/>
                    <input type="button" name="" value="GO" id="checkPinCode" class="search-btn"/>
                </div>
            </div>
            <div class="col-md-4" style="position:relative;">
                <div class="whenyou">
                    <h3>READY WHEN YOU ARE</h3>
                    <p>Buy Fresh Fruits & Vegetables at Best Price in Delhi-NCR India.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
                    $(document).ready(function(){
                        $('#checkPinCode').click(function(){
                            var pincode = $('#pincode').val();

                            if(pincode == ''){
                                var error = 'Please enter Pincode!';
                            }

                            if(error != null){
                                $('#error-msg').html('');
                                $('#error-msg').html('<b style=\"color:red\">' + error + '</b>');
                            } else {

                                var dataString = 'pincode='+ pincode;
                                $.ajax({
                                    url: 'index.php?route=common/home/checkPincode',
                                    type: 'post',
                                    data: dataString,
                                    success: function(html) {
                                       $('#error-msg').html(html);
                                    }

                                });
                            }

                        })
                    });
                </script>
<div id="popup_iiner" class="popup_remove">
   <!--  <div class="remove_icon"><i class="fa fa-remove"></i></div> -->
<div id="popup">
<!-- <a class="close_1">X</a> -->
    <div class="popup_under">
        <div class="popup_logo">
            <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
        </div>
        <div class="row spacing12">
            <div class="col-sm-4 text-center divider1">
                <div class="text01"></div>
                <p>Same-Day Delivery</p>
                <p>Quality You'll Love</p>
                <p>On Time Every Time</p>
            </div>
            <div class="col-sm-8">
                <div class="row right_searcharea">
                    <div class="col-sm-12 form-group">
                        <label>Your City</label>
                        <select name="" class="inputstyle1">
                            <option value="">Delhi Noida</option>
                            <option value="">Banglore</option>
                            <option value="">Indore</option>
                        </select>
                    </div>
                    <div class="col-sm-12 form-group">
                        <label>Where do you want us to deliver your basket?</label>
                        <input type="text" value="" class="inputstyle2" placeholder="Search by Area or Apartment or Pincode *">
                    </div>
                    <div class="col-sm-12 form-group">
                    <a class="btn start_shpippingbtn">Start Shopping</a>
                    <a class="btn skip_btn">Skip & Explore</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php echo $footer; ?>