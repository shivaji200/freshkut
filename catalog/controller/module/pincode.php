<?php
class ControllerModulePincode extends Controller {
	public function index() {

		$this->load->model('module/pincode');
		//$this->load->model('tool/image');

		if (isset($this->request->get['search'])) {
			$search = $this->request->get['search'];
		} else {
			$search = '';
		}
		if (isset($this->request->get['search'])) {
			$filter_data = array(
				'filter_location'         => $search
			);

			$results = $this->model_module_pincode->getLocations($filter_data);
		}else
		{
			$results = array();
		}

		$data['locations'] = array();
		foreach ($results as $result) {
				$data['locations'][] = array(
					'location_name'  => $result['office_name'],
					'location_id'    => $result['id'],
					'href'       	 => $this->url->link('common/home', 'location=' . $result['office_name'])
				);
			}
		header('Content-Type: application/json');
		header('Content-Type: text/html; charset=utf-8');
		echo json_encode($data['locations']);

	}
}
