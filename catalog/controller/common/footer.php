<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service_guarantee'] = $this->language->get('text_service_guarantee');
		$data['text_ontime'] = $this->language->get('text_ontime');
		$data['text_prefferd_delivery'] = $this->language->get('text_prefferd_delivery');
		$data['text_deliverymin'] = $this->language->get('text_deliverymin');
		$data['text_our_mission'] = $this->language->get('text_our_mission');
		$data['text_customer_service'] = $this->language->get('text_customer_service');
		$data['text_policy'] = $this->language->get('text_policy');
		$data['text_gofresho'] = $this->language->get('text_gofresho');
		$data['text_powered'] = $this->language->get('text_powered');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_about_us'] = $this->language->get('text_about_us');
		$data['text_delivery_info'] = $this->language->get('text_delivery_info');
		$data['text_faq'] = $this->language->get('text_faq');
		$data['text_howworks'] = $this->language->get('text_howworks');
		$data['text_cancel'] = $this->language->get('text_cancel');
		$data['text_terms'] = $this->language->get('text_terms');
		$data['text_privacy'] = $this->language->get('text_privacy');
		$data['text_social'] = $this->language->get('text_social');
		$data['text_contactus'] = $this->language->get('text_contactus');
		$data['text_careers'] = $this->language->get('text_careers');
		$data['text_bulkorder'] = $this->language->get('text_bulkorder');
		$data['text_partner'] = $this->language->get('text_partner');
		$data['text_accont'] = $this->language->get('text_accont');
		$data['address'] = $this->config->get('config_address');
		$data['telephone'] = $this->config->get('config_telephone');
		$data['email'] = $this->config->get('config_email');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

		$data['contact'] = $this->url->link('information/contact', '', 'SSL');
		$data['aboutus'] = $this->url->link('information/about_us', '', 'SSL');
		$data['faq'] = $this->url->link('information/faq', '', 'SSL');
		$data['delivery_info'] = $this->url->link('information/delivery', '', 'SSL');
		$data['how_works'] = $this->url->link('information/how_it_works', '', 'SSL');
		$data['cancel'] = $this->url->link('information/cancel-and-return-policy', '', 'SSL');
		$data['terms'] = $this->url->link('information/terms', '', 'SSL');
		$data['privacy'] = $this->url->link('information/privacy', '', 'SSL');
		$data['social'] = $this->url->link('information/social-media-disclaimer', '', 'SSL');
		$data['careers'] = $this->url->link('information/careers', '', 'SSL');
		$data['bulkorder'] = $this->url->link('information/bulkorder', '', 'SSL');
		$data['return'] = $this->url->link('account/return/add', '', 'SSL');
		$data['sitemap'] = $this->url->link('information/sitemap', '', 'SSL');
		$data['manufacturer'] = $this->url->link('product/manufacturer', '', 'SSL');
		$data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$data['special'] = $this->url->link('product/special', '', 'SSL');
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
		} else {
			return $this->load->view('default/template/common/footer.tpl', $data);
		}
	}
}
