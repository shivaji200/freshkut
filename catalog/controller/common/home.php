<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		//locations
		if (isset($this->request->get['location'])) {
			$this->session->data['location'] = $this->request->get['location'];
			$data['location'] = $this->request->get['location'];
		} elseif(isset($this->session->data['location']) && $this->session->data['location'] != '') {
			$data['location'] = $this->session->data['location'];
		} else {
			$data['location'] = '';
		}
		//end locations

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$data['name'] = $this->config->get('config_name');

		/* Search autocomplete */
		if (isset($this->request->get['search'])) {
			$data['search'] = $this->request->get['search'];
		} else {
			$data['search'] = '';
		}

		$data['search_action'] = $this->url->link('module/pincode', '', 'SSL');
		$data['button_cart'] = $this->language->get('button_cart');
		/* end search autocomplete */

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}
	}

	public function checkPincode() {
		$this->load->language('common/home');

		$this->load->model('module/pincode');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->request->post['pincode']) {
			$check = $this->model_module_pincode->checkPincode($this->request->post['pincode']);
			if($check)
			echo $this->language->get('text_success');
			else
			echo $this->language->get('text_error');

		}

	}
}