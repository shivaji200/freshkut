<?php
class ModelModulePincode extends Model {
	public function checkPincode($pincode) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "pincode WHERE pincode = '" . (int)$pincode . "' AND freshkut_available = 'Available'");

		return $query->row['total'];
	}

	public function getLocations($filter_data) {
		$query = $this->db->query("SELECT *  FROM " . DB_PREFIX . "pincode WHERE freshkut_available = 'Available' AND (pincode LIKE '%" . $this->db->escape($filter_data[filter_location]) . "%' OR office_name LIKE '%" . $this->db->escape($filter_data[filter_location]) . "%') ");
		return $query->rows;
	}
}